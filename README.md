# GA task 2_1

One of my first code (written in 2015) <br>
Here I solve optimisation task:

solve:<br>
*    max(x1^0.5 + x2^0.5 + x3^0.5)<br>
    conditions:<br>
 *       x1^2 + 2*x2^2 + 3 * x3^2 <= 1
         x1, x2, x3 >= 0 