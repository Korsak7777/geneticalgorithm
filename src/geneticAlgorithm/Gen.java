package geneticAlgorithm;

import java.util.List;

public interface Gen {
	
	List<Double> getGen();
	
	/*
	 * Oптимизируемая функция
	 * Возвращает значение, необходимое оптимизировать
	 */
	double optimizableFunction(List<Double> gen);
}
