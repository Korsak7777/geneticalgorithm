package geneticAlgorithm;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.ListIterator;
import java.util.NavigableMap;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.TreeMap;

import Task2_1.task;

public class GeneticEngine <T>{

	private int populationSize;
	private double crossingProbability;
	private double mutationProbability;
	private double crossingQuantity;
	private double mutationQuantity;
	private double evaluationFunctionParametr;
	private int numberOfCycles;
	
	public Random r = new Random();
	
	/*
	 * Конструктор GeneticEngine(размер популяции, вероятность скрещивания, количество скрещиваний,
	 * вероятность мутации, количество мутаций, параметр функции оценки, число циклов)
	 */
	public GeneticEngine(int populationSize, double crossingProbability, 
			double crossingQuantity, double mutationProbability, 
			double mutationQuantity, double evaluationFunctionParametr,
			int numberOfCycles) {
		this.populationSize = populationSize;
		this.crossingProbability = crossingProbability;
		this.crossingQuantity = crossingQuantity;
		this.mutationProbability = mutationProbability;
		this.mutationQuantity = mutationQuantity;
		this.evaluationFunctionParametr = evaluationFunctionParametr;
		this.numberOfCycles = numberOfCycles;
	}
	
	task t = new task();

	/*
	 * Возвращает первую популяцию
	 */
	private List<List<Double>> getFirstPopulation() {
		List<List<Double>> L = new ArrayList<List<Double>>();
		while (L.size()<=populationSize-1) {
			List<Double> g = t.getGen();
			if (t.bounds(g) == true ) {
				L.add(g);
			}
		}
		System.out.println("Первая популяция " + L.size());
		System.out.println(L.toString());
		return L;
	}

	/*
	 * Возвращает гены, после кроссинговера
	 */
	private List<List<Double>> getCrossingGens(List<List<Double>> G) {
			List<List<Double>> sourse = G;
			List<List<Double>> target = new ArrayList<List<Double>>();
			ListIterator<List<Double>> iteratorSourse = sourse.listIterator();

			while (iteratorSourse.hasNext()) {
				List<Double> x = iteratorSourse.next();
				if (r.nextDouble()<crossingProbability) {
					target.add(x);
					iteratorSourse.remove();
				}
			}
			if (target.isEmpty()) {
				return sourse;
			}
			ListIterator<List<Double>> iteratorTarget = target.listIterator();
			if (target.size()%2!=0) {
				sourse.add(iteratorTarget.next());
				iteratorTarget.remove();
			}
			while (iteratorTarget.hasNext()) {		
				List<Double> i1 = iteratorTarget.next();
				List<Double> i2 = iteratorTarget.next();
				for (int i = 0; i<=crossingQuantity; i++) {
					ListIterator<List<Double>> iterator = 
							crossing(i1,i2).listIterator();
					while (iterator.hasNext()) {
						sourse.add(iterator.next());
					}
				}
			}		
			System.out.println("После кроссинговера " + sourse.size());
			System.out.println(sourse.toString());
			return sourse;
	}	
	
	/*
	 * Кроссинговер, возвращает 0, 1 или 2 скрещенных гена
	 */
	private List<List<Double>> crossing(List<Double> target1, List<Double> target2) {
		ListIterator<Double> iterator1 = target1.listIterator();
		ListIterator<Double> iterator2 = target2.listIterator();
		List<List<Double>> result = new ArrayList<List<Double>>();
		List<Double> hair1 = new ArrayList<Double>();
		List<Double> hair2 = new ArrayList<Double>();
		while (iterator1.hasNext()) {
			double random = r.nextDouble();
			double i1 = iterator1.next();
			double i2 = iterator2.next();
			hair1.add(i1*random+i2*(1-random));
			hair2.add(i1*(1-random)+i2*random);
		}
		if (t.bounds(hair1) == true) {
			result.add(hair1);
		}
		if (t.bounds(hair2) == true) {
			result.add(hair2);
		}
		//System.out.println("Кроссинговер");
		return result;		
	};
	
	/*
	 * Возвращает Гены, после мутации
	 */
	private List<List<Double>> getMutationGens(List<List<Double>> G) {		
		List<List<Double>> sourse = G;
		List<List<Double>> target = new ArrayList<List<Double>>();
		ListIterator<List<Double>> iteratorSourse = sourse.listIterator();

		while (iteratorSourse.hasNext()) {
			List<Double> x = iteratorSourse.next();
			if (r.nextDouble()<mutationProbability) {
				target.add(x);
				iteratorSourse.remove();
			}
		}
		ListIterator<List<Double>> iteratorTarget = target.listIterator();
		while (iteratorTarget.hasNext()) {
			List<Double> gen = iteratorTarget.next();
			for (int i = 0; i<=mutationQuantity; i++) {
				List<Double> mutant = mutation(gen);
				if (mutant!=null) {
					sourse.add(mutant);
				}				
			}
		}
		System.out.println("После мутации " + sourse.size());
		System.out.println(sourse.toString());
		return sourse;
	}
	
	/*
	 * Мутация, возвращает 0 или 1 ген после мутации
	 */
	private List<Double> mutation(List<Double> target) {
		ListIterator<Double> iterator = target.listIterator();
		List<Double> mutant = new ArrayList<Double>();
		while (iterator.hasNext()) {
			boolean d0 = r.nextBoolean();
			double d;
			if (d0==true) {
				d=1;
			} else {
				d=-1;
			}
			double M = 0.1;//Должна определяться в task
			mutant.add(iterator.next()+M*d);
		}
		//System.out.println("Мутация");
		if (t.bounds(mutant) == true) {
			return mutant;
		} else {
			return null;
		}
	};
	
	/*
	 * Селекция
	 */
	private List<List<Double>> selection(List<List<Double>> G) {
		List<List<Double>> population = G;
		population.sort((gen1,gen0)->{
			List<Double> gen0L, gen1L;
			gen0L = gen0;
			gen1L = gen1;
			ListIterator<Double> genItarg0L = gen0L.listIterator();
			ListIterator<Double> genItarg1L = gen1L.listIterator();		
			return Double.compare((Math.pow(genItarg0L.next(),0.5)
					+Math.pow(genItarg0L.next(),0.5)
					+Math.pow(genItarg0L.next(),0.5)),
					(Math.pow(genItarg1L.next(),0.5)
							+Math.pow(genItarg1L.next(),0.5)
							+Math.pow(genItarg1L.next(),0.5)));
		});
		ListIterator<List<Double>> listIterPop = population.listIterator();
		NavigableMap<Double, List<Double>> M = new TreeMap<Double, List<Double>>();
		double a = evaluationFunctionParametr;
		while (listIterPop.hasNext()) {
			try {
				M.put(M.lastKey()+a*Math.pow(1-a,(listIterPop.nextIndex())), listIterPop.next());
			} catch (NoSuchElementException e) {
				M.put(a*Math.pow(1-a,(listIterPop.nextIndex())), listIterPop.next());
			}		
		}
		List<List<Double>> list  = new ArrayList<List<Double>>();
		System.out.println(M.toString());
		for (int i = 0; i<populationSize; i++) {
			double rand = r.nextDouble()*M.lastKey();
			list.add(M.get(M.ceilingKey(rand)));
		}
		System.out.println("Селекция " + list.size());
		System.out.println(list.toString());
		return list;
	}
	
	/*
	 * Запускает, прогоняет генетический алгоритм
	 */
	public void run() {
		List<List<Double>> c0 = selection(getMutationGens(getCrossingGens(getFirstPopulation())));//Первый цикл
		Deque<List<List<Double>>> dequeOfPopulations = new ArrayDeque<List<List<Double>>>();
		dequeOfPopulations.add(c0);		
		for (int i = 2;i<=numberOfCycles;i++) {
			dequeOfPopulations.addFirst(selection(getMutationGens(getCrossingGens(dequeOfPopulations.removeLast()))));
		}
	}
	
	/*
	 * Возвращает Гену-победителя
	 */
	public List<Double> finalBestGen(List<List<Double>> G) {
		List<List<Double>> population = G;
		System.out.println("Победитель");
		return population.get(0);
	}

}
