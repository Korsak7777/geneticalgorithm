import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.TreeMap;

public class Task2_1 {

public Random r = new Random();
public int popSize;
public double a;
public double chrosProb;
public double mutProb;


public Task2_1(double a, int popSize, double chrosProb, double mutProb) {
	this.a = a;
	this.popSize = popSize;
	this.chrosProb = chrosProb;
	this.mutProb = mutProb;
}
	
public List<Double> chromosome(){
	double x1 = r.nextDouble();
	double x2 = r.nextDouble();
	double x3 = r.nextDouble();

	List<Double> L = new ArrayList<Double>();
	
	if ((x1<0||x2<0||x3<0)&(Math.pow(x1,2)+2*Math.pow(x2,2)+3*Math.pow(x3,2)>1)) {
		return null;
	} else {
		L.add(x1); L.add(x2); L.add(x3);
	}	
	return L;
}

public boolean chromosomeRight(List<Double> L) {
	try {
		ListIterator<Double> l = L.listIterator();
		double x1 = l.next();
		double x2 = l.next();
		double x3 = l.next();	
		if ((x1<0||x2<0||x3<0)&(Math.pow(x1,2)+2*Math.pow(x2,2)+3*Math.pow(x3,2)>1)) {
			return false;
		} else {
			return true;
		}
	} catch(NoSuchElementException e) {
		return false;
	}
}

public List<List<Double>> population(){
	List<List<Double>> L = new ArrayList<List<Double>>();
	while (L.size()<=popSize-1) {
		if (!chromosome().isEmpty()) {
			L.add(chromosome());
		}		
	}
	System.out.println("Популяция: " +L.toString());
	return L;
	
}

public List<List<Double>>  crosingover(){
	List<List<Double>> P = population();
	ListIterator<List<Double>> listIterPop = P.listIterator();//Итератор популяции
	List<List<Double>> L = new ArrayList<List<Double>>();
	System.out.println(P.toString());
	while (listIterPop.hasNext()) {
		List<Double> l = listIterPop.next();
		if (r.nextDouble()<chrosProb) {
			L.add(l);
			listIterPop.remove();
		} 
	}
	System.out.println(P.toString());
	System.out.println(L.toString());
	ListIterator<List<Double>> listIterL = L.listIterator(); //Итератор хромосом для кроссинговера
	if (L.isEmpty()) {
		return P;
	}
	if (L.size()%2!=0) {
		P.add(listIterL.next());
		listIterL.remove();
	}
	while (listIterL.hasNext()) {
		ListIterator<Double> i1 = listIterL.next().listIterator();//Итератор ДНК
		ListIterator<Double> i2 = listIterL.next().listIterator();
		List<Double> X = new ArrayList<Double>();		
		List<Double> Y = new ArrayList<Double>();
		double c = r.nextDouble();
	}
	return P;
}

public Map<Double, List<Double>> selection() {
	List<List<Double>> P = population();
	P.sort((arg1,arg0)->{
		List<Double> arg0L, arg1L;
		arg0L=arg0;
		arg1L=arg1;
		ListIterator<Double> listItarg0L = arg0L.listIterator();
		ListIterator<Double> listItarg1L = arg1L.listIterator();
		double x0 = listItarg0L.next()*1000;
		double y0 = listItarg0L.next()*1000;
		double z0 = listItarg0L.next()*1000;
		double x1 = listItarg1L.next();
		double y1 = listItarg1L.next();
		double z1 = listItarg1L.next();
		System.out.println("x0: "+ x0 + " " + Math.pow(x0,(0.5)));
		System.out.println("y0: "+ y0 + " " + Math.pow(y0,1/2));
		System.out.println("z0: "+ z0 + " " + Math.pow(z0,1/2));
		System.out.println("x1: "+ x1);
		System.out.println("y1: "+ y1);
		System.out.println("z1: "+ z1);
		double a = (Math.pow(x0,1/2)+Math.pow(y0,1/2)+Math.pow(z0,1/2));
		double b = (Math.pow(x1,1/2)+Math.pow(y1,1/2)+Math.pow(z1,1/2));
		System.out.println("a: "+ a);
		System.out.println("b: "+ b);
		return Double.compare(a,b);
	});	
	System.out.println("После селекции: " +P.toString());
	ListIterator<List<Double>> listIterPop = P.listIterator();
	NavigableMap<Double, List<Double>> M = new TreeMap<Double, List<Double>>();
	while (listIterPop.hasNext()) {
		try {
			M.put(M.lastKey()+a*Math.pow(1-a,(listIterPop.nextIndex())), listIterPop.next());
		} catch (NoSuchElementException e) {
			M.put(a*Math.pow(1-a,(listIterPop.nextIndex())), listIterPop.next());
		}		
	}
	return M;
}
}