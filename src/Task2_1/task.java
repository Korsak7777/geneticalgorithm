package Task2_1;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Random;

import geneticAlgorithm.Gen;
import geneticAlgorithm.fitnesFunction;

public class task implements Gen, fitnesFunction {

	private Random r = new Random();
	
	public task() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean bounds(List<Double> gen) {
		List<Double> L = new ArrayList<Double>();
		L = gen;
		try {
			ListIterator<Double> l = L.listIterator();
			double x1 = l.next();
			double x2 = l.next();
			double x3 = l.next();	
			if ((x1<0|x2<0|x3<0)|(Math.pow(x1,2)+2*Math.pow(x2,2)+3*Math.pow(x3,2)>1)) {
				return false;
			} else {
				return true;
			}
		} catch(NoSuchElementException e) {
			return false;
		}
	}

	@Override
	public double optimizableFunction(List<Double> gen) {
		List<Double> L = new ArrayList<Double>();
		L = gen;
		ListIterator<Double> listIter = 
				L.listIterator();		
		return (Math.pow(listIter.next(),1/2)+
				Math.pow(listIter.next(),1/2)+
				Math.pow(listIter.next(),1/2));
	}

	@Override
	public List<Double> getGen() {
		double x1 = r.nextDouble();
		double x2 = r.nextDouble();
		double x3 = r.nextDouble();
		
		List<Double> L = new ArrayList<Double>();
		L.add(x1); L.add(x2); L.add(x3);
		return L;
	}

}
