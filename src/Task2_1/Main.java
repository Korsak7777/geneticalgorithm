package Task2_1;

import geneticAlgorithm.GeneticEngine;

public class Main {
	
	public static void main(String[] args) {
		GeneticEngine  geneticEngine  = new GeneticEngine (30, 0.3, 2, 0.2, 2, 0.05, 400);
		geneticEngine.run();
	}

}
